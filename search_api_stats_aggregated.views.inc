<?php
function search_api_stats_aggregated_views_data() {

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['search_api_stats_aggregated']['table']['group']  = t('Search API stats aggregated');


  // For other base tables, explain how we join
  $data['search_api_stats_aggregated']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );




  $default_int = array(
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $default_string = array(
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $default_date = array(
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );



  // ----------------------------------------------------------------
  // search_api_stats_aggregated table -- fields

  $data['search_api_stats_aggregated']['qid'] = array(
    'title' => t('Primary Key'),
    'help' => t(''), // The help that appears on the UI,
  ) + $default_int;





  $data['search_api_stats_aggregated']['s_name'] = array(
    'title' => t('Server name'),
    'help' => t(''),
  ) + $default_string;


  $data['search_api_stats_aggregated']['i_name'] = array(
    'title' => t('Index machine_name'),
    'help' => t(''),
  ) + $default_string;


  $data['search_api_stats_aggregated']['timestamp'] = array(
    'title' => t('Timestamp'), // The item it appears as on the UI,
    'help' => t('Unix timestamp of when query occurred.'), // The help that appears on the UI,
  ) + $default_date;



  $data['search_api_stats_aggregated']['cnt'] = array(
    'title' => t('Number of the keyword been searched'),
    'help' => t('Number of times whe this particular keyword has been searched.'),
  ) + $default_int;



  $data['search_api_stats_aggregated']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The {users}.uid of the user who triggered the query.'),
  ) + $default_int;


  $data['search_api_stats_aggregated']['keywords'] = array(
    'title' => t('Keywords'),
    'help' => t('Query keywords arguments.'),
  ) + $default_string;





  $data['search_api_stats_aggregated']['table']['base'] = array(
    'field' => 'qid',
    'title' => t('Search API aggregated stats'),
    'help' => t("Search API aggregated stats."),
    'weight' => -9,
  );

  // Relationship to the 'Users' table
  $data['search_api_stats_aggregated']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('Relationship to users table'),
    'relationship' => array(
      'label' => t('User ID'),
      'base' => 'users',
      'base field' => 'uid',
      // This allows us to not show this relationship if the base is already
      // user so users won't create circular relationships.
      'skip base' => array('users'),
    ),
  );

  return $data;
}

