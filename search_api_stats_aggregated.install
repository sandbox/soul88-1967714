<?php
/**
 * @file
 * Install, update and uninstall functions for the search_api_stats module.
 *
 */

/**
 * Implements hook_schema().
 */
function search_api_stats_aggregated_schema() {
  $schema['search_api_stats_aggregated'] = array(
    'description' => 'Table that contains a log of Search API queries and performance.',
    'fields' => array(
      'qid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique log ID.',
      ),
      's_name' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'description' => 'Search API server machine_name',
      ),
      'i_name' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'description' => 'Search API index machine_name',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when query occurred.',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid of the user who triggered the query.',
      ),
      'keywords' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
        'default' => '',
        'description' => 'Query keywords arguments.',
      ),
      'cnt' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Number of results.',
      ),
    ),
    'primary key' => array('qid'),

    'unique keys' => array(
      'keywords' => array('i_name', 'keywords'),
    ),

  );

  return $schema;
}